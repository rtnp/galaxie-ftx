variable "packer_ssh_username" {
  type    = string
  default = "root"
}

variable "packer_ssh_password" {
  type    = string
  default = "debian42"
}

variable "packer_image_name" {
  type    = string
  default = "debian"
}

variable "packer_image_version" {
  type    = string
  default = "11"
}

variable "packer_config_file" {
  type    = string
  default = "debian11-preseed.cfg"
}

source "qemu" "debian11" {
  accelerator      = "kvm"
  
  boot_command     = ["<esc><wait>", "auto <wait>", "console-keymaps-at/keymap=us <wait>", "console-setup/ask_detect=false <wait>", "debconf/frontend=noninteractive <wait>", "debian-installer=en_US <wait>", "fb=false <wait>", "install <wait>", "kbd-chooser/method=us <wait>", "keyboard-configuration/xkb-keymap=us <wait>", "locale=en_US <wait>", "netcfg/get_hostname=${var.packer_image_name}${var.packer_image_version} <wait>", "preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/http/${var.packer_config_file} <wait>", "<enter><wait>"]
  boot_wait        = "45s"

  disk_cache       = "none"
  disk_compression = true
  disk_discard     = "unmap"
  disk_interface   = "virtio"
  disk_size        = "40000"

  format           = "qcow2"
  headless         = true

  host_port_min    = 2222
  host_port_max    = 2229
  http_port_min    = 10082
  http_port_max    = 10089
  http_directory   = "../../preseeds"

  iso_checksum     = "sha256:e482910626b30f9a7de9b0cc142c3d4a079fbfa96110083be1d0b473671ce08d"
  iso_url          = "https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.6.0-amd64-netinst.iso"

  net_device       = "virtio-net"
  output_directory = "../../../artifacts/qemu/debian11"

  qemu_binary      = "/usr/bin/qemu-system-x86_64"
  qemuargs         = [["-m", "2048M"], ["-smp", "2"]]

  ssh_username     = var.packer_ssh_username
  ssh_password     = var.packer_ssh_password
  ssh_wait_timeout = "90m"

  shutdown_command = "echo '${var.packer_ssh_password}' | sudo -S shutdown -P now"
}


build {
  sources = ["source.qemu.debian11"]
  
  provisioner "shell" {
    execute_command = "{{ .Vars }} bash '{{ .Path }}'"
    inline          = ["apt-get update && apt-get install -y python3"]
  }
  
  provisioner "ansible" {
    playbook_file = "../../../playbooks/setup.yml"
  }

}