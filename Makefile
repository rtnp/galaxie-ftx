SHELL=/bin/bash

.PHONY: install-python
install-python: ## Install python dependencies
	@echo -e "${BLUE}==> Install Python requirements packages${COLOR_OFF}"
	apt install -y python3 python3-pip python3-venv

##
## —————————————————————————— ENVIRONMENTS CONFIGURATION ——————————————————————————
##

.PHONY: env
env: header ## Prepare environment
	@[ -d "${PWD}/.direnv" ] || (echo "Venv not found: ${PWD}/.direnv" && exit 1)
	@pip3 install -U pip --no-cache-dir --quiet &&\
	echo "[  ${Green}OK${Color_Off}  ] ${Yellow}INSTALL${Color_Off} pip3" || \
	echo "[${Red}FAILED${Color_Off}] ${Yellow}INSTALL${Color_Off} pip3"

	@pip3 install -U wheel --no-cache-dir --quiet &&\
	echo "[  ${Green}OK${Color_Off}  ] ${Yellow}INSTALL${Color_Off} wheel" || \
	echo "[${Red}FAILED${Color_Off}] ${Yellow}INSTALL${Color_Off} wheel"

	@pip3 install -U setuptools --no-cache-dir --quiet &&\
	echo "[  ${Green}OK${Color_Off}  ] ${Yellow}INSTALL${Color_Off} setuptools" || \
	echo "[${Red}FAILED${Color_Off}] ${Yellow}INSTALL${Color_Off} setuptools"

	@pip install -U --no-cache-dir -q -r requirements.txt &&\
	echo "[  ${Green}OK${Color_Off}  ] ${Yellow}INSTALL${Color_Off} PIP REQUIREMENTS" || \
	echo "[${Red}FAILED${Color_Off}] ${Yellow}INSTALL${Color_Off} PIP REQUIREMENTS"

.PHONY: prepare
prepare: header ## Install ansible-galaxy requirements
	@echo "***************************** ANSIBLE REQUIREMENTS *****************************"
	@ansible-galaxy install -fr ${PWD}/requirements.yml

.PHONY: header
header: ## Display some information about local machine
	@echo "********************************** PACKER MAKEFILE *****************************"
	@echo "HOSTNAME	`uname -n`"
	@echo "KERNEL RELEASE 	`uname -r`"
	@echo "KERNEL VERSION 	`uname -v`"
	@echo "PROCESSOR	`uname -m`"
	@echo "********************************************************************************"

##
## —————————————— PACKER ——————————————————————————————————————————————————————————
##
is_sudo: 
	@ if [ "${USER}" != "root" ]; then echo "You must use this programme with sudo privileges" && exit 1; fi

.PHONY: deploy-qemu
deploy-qemu: is_sudo ## Create QEMU image
	@echo -e "${BLUE}==> Setup QEMU packer image${COLOR_OFF}"
	@cd ${PACKER_QEMU_DIR} &&\
	packer validate debian11.pkr.hcl &&\
	packer build 	debian11.pkr.hcl

##
## —————————————— CLEAN ———————————————————————————————————————————————————————————
##
.PHONY: clean
clean:
	@ echo -e "${GREEN}--> Clean Environment${COLOR_OFF}"
	@ rm -rf .direnv
	@ rm -rf roles
	@ rm -rf artifacts
